/**
 * @Author: Allen C <chenillen>
 * @Date:   2018-07-06T10:40:42+08:00
 * @Email:  chenillen@gmail.com
 * @Last modified by:   chenillen
 * @Last modified time: 2018-08-20T11:12:17+08:00
 * @Copyright: Copyright (c) 2018 by Allen C.(@chenillen). All Rights Reserved.
 */

import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Produce from './views/Produce.vue';
import Copyright from './views/Copyright.vue';
import Camp from './views/education/Camp.vue';
import Dkjt from './views/education/Dkjt.vue';
import Hcmt from './views/education/Hcmt.vue';
import Lecture from './views/education/Lecture.vue';
import Mixing from './views/education/Mixing.vue';
import Proanswers from './views/education/Proanswers.vue';
import Apps from './views/Apps.vue';
import About from './views/About.vue';
import Contact from './views/Contact.vue';

Vue.use(Router);

export default new Router({
  linkActiveClass: '',
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/produce',
      name: 'produce',
      component: Produce,
    },
    {
      path: '/copyright',
      name: 'copyright',
      component: Copyright,
    },
    {
      path: '/edu/mixing',
      name: 'mixing',
      component: Mixing,
    },
    {
      path: '/edu/camp',
      name: 'camp',
      component: Camp,
    },
    {
      path: '/edu/dkjt',
      name: 'dkjt',
      component: Dkjt,
    },
    {
      path: '/edu/hcmt',
      name: 'hcmt',
      component: Hcmt,
    },
    {
      path: '/edu/lecture',
      name: 'lecture',
      component: Lecture,
    },
    {
      path: '/edu/proanswers',
      name: 'proanswers',
      component: Proanswers,
    },
    {
      path: '/app',
      name: 'apps',
      component: Apps,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact,
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    }
  }
});
